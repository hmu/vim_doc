# vim8 install方法

update方法の対象OSはmacのみです。

brewからvimをインストール可能です。インストールには数分かかります。

```sh
> brew update
> brew install vim
```

もし[brew](https://brew.sh/index_ja.html)コマンドがインストールされていなかった場合は、以下の行を実行してインストールしてください。

```sh
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
----

# vimを使うにあたり最初やること

- osの設定で、キーリピート間隔とカーソル移動速度を最速にしましょう。
- macの場合、karabinerを利用することで、os設定以上に高速化できるのでおすすめです。

----

# vimを使うとどうなる？

- キーボードとマウスの行き来が減ります。
- 自分の好きなようにカスタマイズできます。
- 他人のエディタにさわれなくなります。

----
# vimの慣れ方

とにかく使うこと。使って不便に思ったことをググること。大抵解決策が出てくるので、使う毎に使いやすくなっていきます。

----
# vimの基本

vimには、大まかに３つのモードがあります。

- カーソル移動などを行うための**ノーマルモード**
- 文字を入力するための**インサートモード**
- ファイルオープンや検索,置換などを行うための**コマンドモード**

----

通常のモードはカーソル移動をしたりする「**ノーマルモード**」です。文字を入力する場合のみ「**インサートモード**」に切り替えます。

「**ノーマルモード**」から「**インサートモード**」に切り替えるには、*i*キーを押します。「**インサートモード**」になると、左下に-- INSERT --が表示され、文字を入力できるようになります。

「**インサートモード**」から「**ノーマルモード**」に復帰するには、*ESC*キーまたは*CTRL+[*キーを押します。*ESC*は押しづらいので*CTRL+[*をオススメします。

vimを終了するには、「**ノーマルモード**」から*:*キーを押して「**コマンドモード**」に切り替えます。左下に:が表示されるので、*q!*と入力してエンターを押すと終了します。

----

## helpが充実している

コマンドモードで **:help** と入力すると、vimのヘルプを閲覧することができます。

特定の機能について調べるんは、 **:help number** などとhelpに続けてキーワードを入力します。

**:q**でヘルプを閉じることができます。

## カーソル移動の基本操作

|key|内容|
|--:|:--|
|h|左|
|j|下|
|k|上|
|l|右|
|CTRL+d|下にスクロール|
|CTRL+u|上にスクロール|
|行頭|0|
|文頭|_|
|行末|$|
|H|ページの先頭へ|
|M|ページの中央へ|
|L|ページの末尾へ|
|CTRL+e|カーソル位置はそのままで、１行上にスクロールする|
|CTRL+y|カーソル位置はそのままで、１行下にスクロールする|
|*|カーソル位置の単語をインクリメンタルサーチ（下向き）|
|#|カーソル位置の単語をインクリメンタルサーチ（上向き）|

## 編集の基本操作

|key|内容|
|--:|:--|
|dd|カーソル行を削除|
|u|アンドゥ|
|CTRL+r|リドゥ|
|d1j|カーソル行から２行削除|
|dw|カーソル位置からの単語を削除|
|cw|カーソル位置からの単語を削除して、インサートモードへ|
|D|カーソル位置から行末までを削除|
|C|カーソル位置から行末までを削除して、インサートモードへ|
|d0|カーソル位置から行頭までを削除|
|x|カーソル位置の一文字を削除|
|X|カーソル位置の一文字前を削除|
|>>|右にインデントをつける|
|<<|左にインデントをつける|
|yy|カーソル行をヤンク(コピー）する|
|p|ヤンクされた行をペーストする|
|CTRL+==|コメントアウト切り替え|
|SHIFT+==|インデント調整|

## 表示設定変更の基本

"行番号を表示する"、2表示色を変更する"などの設定は、コマンドモードで行います。

|command|内容|
|--:|:--|
|set number|行番号を表示する|
|set nonumber|行番号を非表示にする|
|set hlsearch|検索文字をハイライトする|
|set nohlsearch|検索文字のハイライトを止める|
|set tabstop=n|タブサイズをn(1,2,3...)にする|
|set shiftwidth=n|>>でのインデントサイズをn(1,2,3...)にする|
|set expandtab|タブを空白に変換する|
|set autoindent|オートインデントを有効にする|
|set clipboard+=unnamed|ヤンクした内容をクリップボードにコピーする|

## vimの設定方法

vimの設定を変更するためには、~/.vimrcファイルを作成し、そこに設定を記述します。

```vimscript
set nocompatible  " be iMproved

set hlsearch      " 検索文字のハイライト
set tabstop=2     " タブ文字を2文字にする
set shiftwidth=2  " インデント幅を2文字にする
set expandtab     " タブを空白に展開する
set autoindent    " オートインデントを有効にする
set number        " 行番号を表示する

inoremap <C-j> <ESC> " Ctrl+jでESCを行う


```

# Vundleによるplugin管理

pluginマネージャを利用することで、簡単にpluginを導入することができます。
色々なマネージャーがありますが、ここでは簡単な[Vundle](https://github.com/VundleVim/Vundle.vim)の設定例を紹介します。

Vundleをgit cloneでインストールします。

```sh
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

.vimrcに以下を追記します。

```vimscript

filetype off      " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" << この間にインストールするpluginを記述します。

Plugin 'tpope/vim-fugitive'      "git
Plugin 'neowit/vim-force.com'    "vim-force
Plugin 'majutsushi/tagbar'       "tagbar
 
" >> ここまで


call vundle#end()            " required
filetype plugin indent on    " required
```

vimプラグインをインストールするには、一度vim再起動して.vimrcの変更を適用した上で、
コマンドモードで以下を実行します。

```vim
:BundleInstall
```

----

# Vimプラグイン

## ファイルマネージャー

### [VimFiler](https://github.com/Shougo/vimfiler.vim)

Vimでファイル一覧を表示します。

#### 設定例

```vim
Plugin 'Shougo/vimfiler' "for install

```

#### 使い方

```vim
:VimFiler " カレントディレクトリのファイル一覧を表示する
:VimFilerBufferDir " 現在のファイルのファイル一覧を表示する
```



